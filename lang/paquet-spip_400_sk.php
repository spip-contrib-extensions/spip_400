<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-spip_400?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spip_400_description' => 'Tento zásuvný modul dopĺňa distribúciu SPIPu tým, že ponúka šablóny chybových stránok HTTP ({kódy 401 a 404}) s vysvetlivkou a možnosťou pre používateľa poslať webmasterovi stránky "lístok s chybou".

Ponúka najmä:
-* správu na verejne prístupných stránkach, aby sa používateľ nestratil,
-* poslanie e-mailu webmasterovi s kompletnými informáciami o danej chybe ({používateľ SPIPu, URL, backtrace PHP, atď.}),
-* zápis správ do špeciálneho súboru LOG.',
	'spip_400_nom' => 'SPIP 400',
	'spip_400_slogan' => 'Ovládanie vynútených chybových stránok (401, 404) pre SPIP'
);
